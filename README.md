# Part 2A - implementation 

## The code is badly structured (everything mixed in one file): Make a note of what's wrong with the structure (and anything else you see that may be problematic) - could any of these things have a secrity impact?

Sensitive/confidential data stored in app.py:
* The username and passwords for login are stored in a single dictionary inside the app.py itself. Anyone with access to the source code would therefore be able to log in.
* Secret key.
Solution: Put the confidential secret key in a config file that gets loaded into the app.py file. The username and passwords are also stored in the config file. A database would have been even more secure, but I did not do that.
The old secret key is also not a good one. It is easy to guess/brute-force and I generated a new more secure one to use instead.

I removed the `inject` variable, I don't want to give potential attackers any help.

The program does not check passwords, anyone can login as alice or bob. This is a massive security risk.

I'm sure the product has a lot of functions that should have been loaded in from separate files instead, like the functions executing SQL commands.



#### I ran out of time, so here is a list of things I wanted to implement but ultimately could not:
* Password checking. Finding a secure way to store passwords (i.e., with hashing and salt).
* Put the user database in an actual database (not just as a dict).
* Configure the way session data is stored in order to know who's logged in. 
* Have the username be automatic and unchangeably tied to the chat message. This way whoever is logged in cannot assume someone elses identity.
* To not have the chat re-posted, but rather look as if new messages smoothly gets added to the bottom of the chat as they are sendt.

### Notes about the final product:
* The program is still far from what I would call secure.
* It is most certainly vulnerable to SQL injections. Injections similar to what was done in the tiny-server task way back should be effective here as well.

-------

# Part 2B - Documentation
A brief overview of my design considerations from Part A:
* See Part A above.

The features of my application
* This program does not really function on multiple tabs (especially not over a network or anything like that).
You can at least pretend like you are two different users by writing a different username one after another. This way you could send messages between "Alice" and "Bob", for example.
* If you wish to logout you can type /logout at the end of the URL.

Instructions on how to test/demo it:
* To get it to run you need `pip install flask flask_wtf flask_login`, and `flask run`.
* You will be greeted by a login screen. The usernames and passwords are located in config.py. However typing alice or bob in username will work.
* Once you are logged in you will arrive at the index.html screen. Here is a chat box where you can choose the name of the User and a message. Press `send` to see your message be added to the chat.

Technical details on the implementation:
* I altered the search()-function in app.py. Now it shows the contents of the database in a more user-friendly way. I tried to only display username and message as to mimic a chat window.


#### Answers to the questions below:
Threat model - who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?

* The attacker from a confidentiality point of view would try to steal sensitive information from the chat. 
In terms of availability he could overload the database with messages turning the chat unusable.
In terms of integrity he could permanently delete information from the database. All are very undesirable outcomes.
In my websites' case the attacker can do a lot.

What are the main attack vectors for the application?
* ...

What should we do (or what have you done) to protect against attacks?

* What I have done is move sensitive data to ca onfig file and strengthen the secret_key. I have a login page, but it could be more secure. 
What you should do to protect against attacks is make sure the authorization is strict. The program should know who uses it and not freely give access purely based on typing in the correct URL or username only. The SQL queries should be more resilient to injection attacks.

What is the access control model?

* I would assume it is RBAC.

How can you know that your security is good enough? (traceability)

* By using application logging. It provides invaluable data for analysing how secure your application is.
